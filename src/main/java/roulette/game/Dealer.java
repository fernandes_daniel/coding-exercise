package roulette.game;

import roulette.bet.Bet;
import roulette.input.BetTaker;
import roulette.output.ResultsBoard;
import roulette.roulette.Roulette;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Dealer {
    private Roulette roulette;
    private final ResultsBoard resultsBoard;
    private final BetTaker betTaker;
    private final PlayersHashMap listOfRegisteredPlayers;
    private List<Bet> betList = new ArrayList<Bet>();

    public Dealer( Roulette roulette, ResultsBoard resultsBoard, BetTaker betTaker, PlayersHashMap listOfRegisteredPlayers ) {
        this.roulette = roulette;
        this.resultsBoard = resultsBoard;
        this.betTaker = betTaker;
        this.listOfRegisteredPlayers = listOfRegisteredPlayers;
    }

    public void spinRoulette() {
        synchronized ( betList ) {
            roulette.spin();
            settleAllBets();
            resultsBoard.displayResults( roulette, betList, listOfRegisteredPlayers );
            betList.clear();
        }
    }

    private void settleAllBets() {
        for ( Bet bet : betList ) {
            bet.settle( roulette );
        }
    }

    public void takeBets() throws IOException, InterruptedException {
        while ( true ) {
            synchronized ( betList ) {
                final Bet bet = betTaker.takeBet( listOfRegisteredPlayers );
                if ( bet != null ) {
                    betList.add( bet );
                }
            }
            Thread.sleep( 500 );
        }
    }

    public void setBetList( List<Bet> betList ) {
        this.betList = betList;
    }

    public List<Bet> getBetList() {
        return betList;
    }
}
