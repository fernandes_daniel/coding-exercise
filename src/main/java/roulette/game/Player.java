package roulette.game;

public class Player {
    final private String name;
    private Float totalWon;
    private Float totalBet;

    public Player( String name, Float totalWon, Float totalBet ) {
        this.name = name;
        this.totalWon = totalWon;
        this.totalBet = totalBet;
    }

    public void setTotalWon( Float totalWon ) {
        this.totalWon = totalWon;
    }

    public void setTotalBet( Float totalBet ) {
        this.totalBet = totalBet;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Player{" +
                "\nname='" + name + '\'' +
                "\n, totalWon=" + totalWon +
                "\n, totalBet=" + totalBet +
                '}';
    }

    public Float getTotalWon() {
        return totalWon;
    }

    public Float getTotalBet() {
        return totalBet;
    }

    @Override
    public boolean equals( Object o ) {
        return toString().equals( o.toString() );
    }
}