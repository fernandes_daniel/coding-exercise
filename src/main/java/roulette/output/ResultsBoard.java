package roulette.output;

import roulette.bet.Bet;
import roulette.game.Player;
import roulette.game.PlayersHashMap;
import roulette.roulette.Roulette;

import java.util.List;

public class ResultsBoard {

    public void displayResults( Roulette roulette, List<Bet> betList, PlayersHashMap players ) {
        System.out.println( String.format( "Number: %d", roulette.getBallNumber() ) );
        System.out.println( "Player Bet Outcome Winnings" );
        System.out.println( "---" );

        for ( Bet bet : betList ) {
            String playersName = bet.getPlayer().getName();
            String betAmountType = bet.getBetTypeOutput();
            String outcome = bet.getOutcomeOutput();
            String winAmount = bet.getWinAmountOutput();

            System.out.println( String.format( "%s %s %s %s", playersName, betAmountType, outcome, winAmount ) );
        }

        System.out.println( "Player Total Win Total Bet" );
        System.out.println( "---" );

        for ( Player player : players.values() ) {
            String playersName = player.getName();
            String totalWins = String.valueOf( player.getTotalWon() );
            String totalBets = String.valueOf( player.getTotalBet() );

            System.out.println( String.format( "%s %s %s", playersName, totalWins, totalBets ) );
        }


    }
}
