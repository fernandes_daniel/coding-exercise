package roulette;

import roulette.game.Dealer;
import roulette.input.BetLineParser;
import roulette.input.BetTaker;
import roulette.input.PlayersFileParser;
import roulette.output.ResultsBoard;
import roulette.roulette.Roulette;

import java.io.File;
import java.io.IOException;
import java.util.Random;

public class DependencyFactory {

    private final File playersFile;

    public DependencyFactory( File playersFile ) {
        this.playersFile = playersFile;
    }

    public Dealer newDealer() throws IOException {
        PlayersFileParser playersFileParser = new PlayersFileParser();
        return new Dealer(
                newRoulette(),
                new ResultsBoard(),
                newBetTaker(),
                playersFileParser.parsePlayers( playersFile )
        );
    }

    public Roulette newRoulette() {
        return new Roulette( new Random() );
    }

    public BetTaker newBetTaker() throws IOException {
        return new BetTaker( newBetLineParser() );
    }

    private BetLineParser newBetLineParser() throws IOException {
        return new BetLineParser();
    }

}
