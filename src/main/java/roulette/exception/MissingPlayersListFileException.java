package roulette.exception;

public class MissingPlayersListFileException extends Exception {
    public MissingPlayersListFileException( String message ) {
        super( message );
    }
}
