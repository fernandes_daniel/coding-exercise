package roulette.roulette;

import java.util.Random;

public class Roulette {
    private final Random intGenerator;
    private int ballNumber;

    public Roulette( Random intGenerator ) {
        this.intGenerator = intGenerator;
        ballNumber = 1;
    }

    public void spin() {
        ballNumber = intGenerator.nextInt( 35 ) + 1;
    }

    public int getBallNumber() {
        return ballNumber;
    }

    public void setBallNumber( int ballNumber ) {
        this.ballNumber = ballNumber;
    }

    public boolean getIsBallEven() {
        return ballNumber % 2 == 0;
    }

    public boolean getIsBallOdd() {
        return !getIsBallEven();
    }
}
