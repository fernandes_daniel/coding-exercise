package roulette.bet;

import roulette.game.Player;
import roulette.roulette.Roulette;

public class EvenOddBet extends Bet {
    final private boolean even;

    public EvenOddBet( Player player, Float amount, boolean even ) {
        super( player, amount );
        this.even = even;
    }

    @Override
    public Float settle( Roulette roulette ) {
        if ( roulette.getIsBallEven() && betIsEven()
                || roulette.getIsBallOdd() && betIsOdd() ) {
            setAmountWon( getBetAmount() * 2 );
            return amountWon;
        }
        setAmountWon( ( float ) 0 );
        return amountWon;
    }

    @Override
    public String getBetTypeOutput() {
        if(even){
            return "EVEN";
        }
        return "ODD";
    }

    public boolean betIsEven() {
        return even;
    }

    public boolean betIsOdd() {
        return !even;
    }

    @Override
    public String toString() {
        return "EvenOddBet{" +
                "\neven=" + even +
                "\nplayer=" + getPlayer().toString() +
                "\nbetAmount=" + getBetAmount() +
                "\namountWon=" + amountWon +
                '}';
    }

    @Override
    public boolean equals( Object o ) {
        return o.toString().equals( this.toString() );
    }

}
