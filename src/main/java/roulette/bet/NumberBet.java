package roulette.bet;

import roulette.game.Player;
import roulette.roulette.Roulette;

public class NumberBet extends Bet {
    final private int betNumber;

    public NumberBet( Player player, Float amount, int betNumber ) {
        super( player, amount );
        this.betNumber = betNumber;
    }

    @Override
    public Float settle( Roulette roulette ) {
        if ( roulette.getBallNumber() == betNumber ) {
            setAmountWon( 36 * getBetAmount() );
            return amountWon;
        }
        setAmountWon( ( float ) 0 );
        return amountWon;
    }

    @Override
    public String getBetTypeOutput() {
        return String.valueOf( betNumber );
    }

    @Override
    public boolean equals( Object o ) {
        return o.toString().equals( this.toString() );
    }

    @Override
    public String toString() {
        return "NumberBet{" +
                "\nbetNumber=" + betNumber +
                "\nplayer=" + getPlayer().toString() +
                "\nbetAmount=" + getBetAmount() +
                "\namountWon=" + amountWon +
                '}';
    }
}
