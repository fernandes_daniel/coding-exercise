package roulette.bet;

import roulette.game.Player;
import roulette.roulette.Roulette;

public abstract class Bet {
    final private Player player;
    final private Float betAmount;
    protected Float amountWon = null;

    protected Bet( Player player, Float betAmount ) {
        this.player = player;
        this.betAmount = betAmount;
    }

    abstract public Float settle( Roulette roulette );

    public Player getPlayer() {
        return player;
    }

    protected Float getBetAmount() {
        return betAmount;
    }

    protected void setAmountWon( Float amountWon ) {
        this.amountWon = amountWon;
        player.setTotalWon( player.getTotalWon() + amountWon );
        player.setTotalBet( player.getTotalBet() + betAmount );
    }

    abstract public String getBetTypeOutput();

    public String getOutcomeOutput() {
        if ( amountWon > 0 ) {
            return "WIN";
        }
        return "LOSE";
    }

    public String getWinAmountOutput() {
        return String.valueOf( amountWon );
    }

}