package roulette;

import roulette.exception.MissingPlayersListFileException;
import roulette.game.Dealer;
import roulette.game.SpinTimer;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Bootstrap {

    public static void main( String[] args ) throws MissingPlayersListFileException, IOException, InterruptedException {
        if ( args.length < 1 ) {
            throw new MissingPlayersListFileException( "First argument should be the full path to " +
                    "file with the list of players" );
        }
        DependencyFactory dependencyFactory = new DependencyFactory( new File( args[ 0 ] ) );

        Dealer dealer = dependencyFactory.newDealer();

        ScheduledExecutorService executor = Executors.newScheduledThreadPool( 1 );
        executor.scheduleWithFixedDelay( new SpinTimer( dealer ), 30, 30, TimeUnit.SECONDS );

        dealer.takeBets();
    }
}
