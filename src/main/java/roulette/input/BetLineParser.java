package roulette.input;

import roulette.bet.Bet;
import roulette.bet.EvenOddBet;
import roulette.bet.NumberBet;
import roulette.game.Player;
import roulette.game.PlayersHashMap;

import java.util.List;

public class BetLineParser {

    public Bet parseBetLine( String betLine, PlayersHashMap listOfRegisteredPlayers ) {
        if ( betLine == null ) {
            return null;
        }

        String[] strings = betLine.split( " " );
        if ( strings.length != 3 ) {
            return null;

        }

        Player player = checkPlayerByName( strings[ 0 ], listOfRegisteredPlayers );
        if ( player == null ) {
            return null;
        }

        Float betAmount = parseBetAmount( strings[ 2 ] );
        if ( betAmount == null ) {
            return null;
        }

        try {
            int betNumber = Integer.parseInt( strings[ 1 ] );
            if ( betNumber >= 1 && betNumber <= 36 ) {
                return new NumberBet( player, betAmount, betNumber );
            }
            return null;
        } catch ( NumberFormatException e ) {

            if ( strings[ 1 ].toUpperCase().equals( "EVEN" ) ) {
                return new EvenOddBet( player, betAmount, true );
            } else {
                if ( strings[ 1 ].toUpperCase().equals( "ODD" ) ) {
                    return new EvenOddBet( player, betAmount, false );
                }
            }
        }

        return null;
    }

    private Player checkPlayerByName( String playerName, PlayersHashMap listOfRegisteredPlayers ) {
        return listOfRegisteredPlayers.get( playerName );
    }

    private Float parseBetAmount( String inputString ) {
        try {
            return Float.parseFloat( inputString );
        } catch ( NumberFormatException e ) {
            return null;
        }
    }
}
