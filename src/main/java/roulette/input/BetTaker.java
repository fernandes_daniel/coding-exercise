package roulette.input;

import roulette.bet.Bet;
import roulette.game.PlayersHashMap;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class BetTaker {
    private final BetLineParser betLineParser;

    public BetTaker( BetLineParser betLineParser) {
        this.betLineParser = betLineParser;
    }

    public Bet takeBet(PlayersHashMap playersHashMap) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Enter New Bet Line:");
        String betLine = br.readLine();

        return betLineParser.parseBetLine( betLine, playersHashMap );
    }
}
