package roulette.input;

import org.apache.commons.io.FileUtils;
import roulette.game.PlayersHashMap;
import roulette.game.Player;

import java.io.File;
import java.io.IOException;

public class PlayersFileParser {

    public PlayersHashMap parsePlayers( File playersFile ) throws IOException {
        String fileContent = FileUtils.readFileToString( playersFile );

        String[] fileRows = fileContent.split( "[\r\n]+" );

        PlayersHashMap listOfPlayers = new PlayersHashMap();
        for ( String row : fileRows ) {
            if ( row.trim().equals( "" ) ) {
                continue;
            }

            String[] columns = row.split( "," );

            Player player = new Player(
                    columns[ 0 ],
                    parseTotalWon( columns ),
                    parseTotalBet( columns )
            );
            listOfPlayers.put( player.getName(), player );
        }

        return listOfPlayers;
    }

    private Float parseTotalWon( String[] columns ) {
        if ( columns.length < 2 ) {
            return ( float ) 0;
        }

        try {
            return Float.parseFloat( columns[ 1 ] );
        } catch ( NumberFormatException e ) {
            return ( float ) 0;
        }
    }

    private Float parseTotalBet( String[] columns ) {
        if ( columns.length < 3 ) {
            return ( float ) 0;
        }

        try {
            return Float.parseFloat( columns[ 2 ] );
        } catch ( NumberFormatException e ) {
            return ( float ) 0;
        }
    }
}
