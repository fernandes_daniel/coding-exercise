package roulette.input;

import org.junit.Before;
import org.junit.Test;
import roulette.bet.Bet;
import roulette.bet.EvenOddBet;
import roulette.bet.NumberBet;
import roulette.game.Player;
import roulette.game.PlayersHashMap;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertEquals;

public class BetLineParserTest {
    private BetLineParser betLineParser;
    private PlayersHashMap listOfPlayers;

    @Before
    public void setUp() throws Exception {
        listOfPlayers = new PlayersHashMap();
        listOfPlayers.put( "Tiki_Monkey", new Player( "Tiki_Monkey", ( float ) 23.2, ( float ) 1 ) );
        listOfPlayers.put( "Barbara", new Player( "Barbara", ( float ) 34, ( float ) 0.5 ) );
        listOfPlayers.put( "Michael", new Player( "Michael", ( float ) 2.1, ( float ) 0 ) );

        betLineParser = new BetLineParser();
    }

    @Test
    public void testParseBetLine_NumberBet() throws Exception {
        Bet expectedBet = new NumberBet( new Player( "Tiki_Monkey", ( float ) 23.2, ( float ) 1 ), ( float ) 1.5, 2 );
        Bet actualBet = betLineParser.parseBetLine( "Tiki_Monkey 2 1.5",listOfPlayers );
        assertEquals( expectedBet, actualBet );
    }

    @Test
    public void testParseBetLine_EvenBet() throws Exception {
        Bet expectedBet = new EvenOddBet( new Player( "Barbara", ( float ) 34, ( float ) 0.5 ), ( float ) 3, true );
        assertEquals( expectedBet, betLineParser.parseBetLine( "Barbara EVEN 3",listOfPlayers  ) );
    }

    @Test
    public void testParseBetLine_OddBet() throws Exception {
        Bet expectedBet = new EvenOddBet( new Player( "Michael", ( float ) 2.1, ( float ) 0 ), ( float ) 456, false );
        assertEquals( expectedBet, betLineParser.parseBetLine( "Michael Odd 456",listOfPlayers  ) );
    }

    @Test
    public void testParseBetLine_InvalidBetFormat() throws Exception {
        assertNull( betLineParser.parseBetLine( "bla Michael Odd 456",listOfPlayers  ) );
    }

    @Test
    public void testParseBetLine_InvalidBetAmount() throws Exception {
        assertNull( betLineParser.parseBetLine( "Michael Odd INVALID_AMOUNT",listOfPlayers  ) );
    }

    @Test
    public void testParseBetLine_InvalidBetType() throws Exception {
        assertNull( betLineParser.parseBetLine( "Michael INVALID_TYPE 2",listOfPlayers  ) );
    }

    @Test
    public void testParseBetLine_PlayerNotRegistered() throws Exception {
        assertNull( betLineParser.parseBetLine( "PlayerNotInGame EVEN 2",listOfPlayers  ) );
    }

}
