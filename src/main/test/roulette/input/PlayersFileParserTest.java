package roulette.input;

import org.junit.Before;
import org.junit.Test;
import roulette.game.Player;
import roulette.game.PlayersHashMap;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class PlayersFileParserTest {

    private PlayersFileParser playersFileParser;

    @Before
    public void setUp() throws Exception {
        playersFileParser = new PlayersFileParser();
    }

    @Test
    public void testParsePlayerNames() throws Exception {
        PlayersHashMap expectedPlayers = new PlayersHashMap();
        expectedPlayers.put( "Tiki_Monkey", new Player( "Tiki_Monkey", ( float ) 2.3, ( float ) 2 ) );
        expectedPlayers.put( "Barbara", new Player( "Barbara", ( float ) 0, ( float ) 3 ) );

        assertEquals(
                expectedPlayers,
                playersFileParser.parsePlayers( new File( "./src/main/test/resources/listOfPlayers.txt" ) )
        );
    }
}
