package roulette.bet;

import org.junit.Test;
import roulette.game.Player;
import roulette.roulette.Roulette;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class EvenOddBetTest {

    @Test
    public void testSettle_OnWin() throws Exception {
        EvenOddBet evenOddBet = new EvenOddBet(
                new Player( "Player Name", ( float ) 0, ( float ) 0 ),
                ( float ) 20,
                true
        );

        Roulette roulette = mock( Roulette.class );
        when( roulette.getIsBallEven() )
                .thenReturn( true );

        Float settleAmount = evenOddBet.settle( roulette );

        assertEquals( ( float ) 20 * 2, settleAmount );

        verify( roulette ).getIsBallEven();
    }
}
