package roulette.game;

import org.junit.Before;
import org.junit.Test;
import roulette.bet.Bet;
import roulette.input.BetTaker;
import roulette.output.ResultsBoard;
import roulette.roulette.Roulette;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class DealerTest {

    private Dealer dealer;
    private Roulette roulette;
    private ResultsBoard resultsBoard;
    private BetTaker betTaker;
    private List<Bet> emptyBetList;
    private PlayersHashMap players;

    @Before
    public void setUp() throws Exception {
        roulette = mock( Roulette.class );
        resultsBoard = mock( ResultsBoard.class );
        betTaker = mock( BetTaker.class );
        players = mock( PlayersHashMap.class );

        dealer = new Dealer( roulette, resultsBoard, betTaker, players );

        emptyBetList = new ArrayList<Bet>();
    }

    @Test
    public void testSpinRoulette() throws Exception {
        List<Bet> betList = new ArrayList<Bet>();
        Bet bet1 = mock( Bet.class );
        betList.add( bet1 );
        Bet bet2 = mock( Bet.class );
        betList.add( bet2 );
        Bet bet3 = mock( Bet.class );
        betList.add( bet3 );

        dealer.setBetList( betList );

        dealer.spinRoulette();

        verify( roulette ).spin();
        verifyNoMoreInteractions( roulette );

        verify( bet1 ).settle( roulette );
        verifyNoMoreInteractions( bet1 );
        verify( bet2 ).settle( roulette );
        verifyNoMoreInteractions( bet2 );
        verify( bet3 ).settle( roulette );
        verifyNoMoreInteractions( bet3 );

        verify( resultsBoard ).displayResults( roulette, betList, players );

        assertEquals( emptyBetList, dealer.getBetList() );
    }
}
