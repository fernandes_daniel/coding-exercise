package roulette.roulette;

import org.junit.Before;
import org.junit.Test;

import java.util.Random;

import static junit.framework.Assert.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

public class RouletteTest {

    private Roulette roulette;
    private Random ballNumberGenerator;

    @Before
    public void setUp() {
        ballNumberGenerator = mock( Random.class );
        roulette = new Roulette( ballNumberGenerator );
    }

    @Test
    public void testSpin() throws Exception {
        when( ballNumberGenerator.nextInt( 35 ) )
                .thenReturn( 19 );

        roulette.spin();

        verify( ballNumberGenerator ).nextInt( 35 );
        assertEquals( 20, roulette.getBallNumber() );

    }

    @Test
    public void testGetIsBallEven() throws Exception {
        roulette.setBallNumber( 2 );
        assertTrue(roulette.getIsBallEven());

        roulette.setBallNumber( 3 );
        assertFalse( roulette.getIsBallEven() );

        roulette.setBallNumber( 10 );
        assertTrue(roulette.getIsBallEven());

        roulette.setBallNumber( 15 );
        assertFalse(roulette.getIsBallEven());
    }
}
